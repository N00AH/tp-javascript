# TP-JavaScript

TP JavaScript  cours

> * Auteur : Nemri Noah
> * Date de publication : 06/04/2021

## Sommaire

## 1. Introduction :

- [Code HelloWorld](Introduction/HelloWorld_JS/index.html)
- [Test du projet HelloWorld](Introduction/readme.md)

## 2. Variable et Opérateur :

**_-Exo surface :_**
- [Code calcul surface (html)](variable_et_operateur/Calcul_Surface/index.html)
- [Code calcul surface (js)](variable_et_operateur/Calcul_Surface/js/surface.js)
- [Test du projet Surface](variable_et_operateur/Calcul_Surface/readme.md)
- [Arborescence](variable_et_operateur/Calcul_Surface/images/arborescence.png)
  
**_-Exo IMC :_**
- [Code calcul imc (html)](variable_et_operateur/Calcul-IMC/index.html)
- [Code calcul imc (js)](variable_et_operateur/Calcul-IMC/js/IMC.JS)
- [Test du projet Surface](variable_et_operateur/Calcul-IMC/readme.md)
- [arborescence](variable_et_operateur/Calcul-IMC/images/arborescence.png)

**_-Exo conversion :_**
- [Code conersion (html)](variable_et_operateur/Convertion_Celcius_Fahrenheit/index.html)
- [Code conersion (js)](variable_et_operateur/Convertion_Celcius_Fahrenheit/js/convertion.js)
- [Test du projet conversion](variable_et_operateur/Convertion_Celcius_Fahrenheit/readme.md)
- [arborescence](variable_et_operateur/Convertion_Celcius_Fahrenheit/images/arborescence.png)

## 3. Les structures de contrôle en JavaScript :

**_-Exo IMC et interprétation :_**
- [Code calcul et interpretation imc (html)](structures_controle_js/Calcul_IMC_plus/index.html)
- [Code calcul et interpretation imc (js)](structures_controle_js/Calcul_IMC_plus/js/calcul_imc.js)
- [Test du projet calcul et interpretation imc](structures_controle_js/Calcul_IMC_plus/readme.md)
- [arborescence](variable_et_operateur/Calcul-IMC/images/arborescence.png)
  
**_-Exo conjecture de syracuse :_**
- [Code conjecture de syracuse (html)](structures_controle_js/Conjecture_Syracuse/index.html)
- [Code conjecture de syracuse (js)](structures_controle_js/Conjecture_Syracuse/js/Conjecture-Syracuse.js)
- [Test du projet conjecture de syracuse](structures_controle_js/Conjecture_Syracuse/readme.md)
- [arborescence](structures_controle_js/Conjecture_Syracuse/images/arborescence.png)

**_-Exo calcul factorielle :_**
- [Code calcul factorielle (html)](structures_controle_js/calcul-factorielle/index.html)
- [Code calcul factorielle (js)](structures_controle_js/calcul-factorielle/js/factorielle.js)
- [Test du projet calcul factorielle](structures_controle_js/calcul-factorielle/readme.md)
- [arborescence](structures_controle_js/calcul-factorielle/images/arborescence.png)

**_-Exo conversion euros dollars :_**
- [Code conversion euros/dollars (html)](structures_controle_js/conversion_euros_dollars/index.html)
- [Code conversion euros/dollars (js)](structures_controle_js/conversion_euros_dollars/js/euros-dollars.js)
- [Test du projet conversion euros/dollars](structures_controle_js/conversion_euros_dollars/readme.md)
- [arborescence](structures_controle_js/conversion_euros_dollars/images/arborescence.png)

**_-Exo nombres triples :_**
- [Code nombres triples (html)](structures_controle_js/nombres_triples/index.html)
- [Code nombres triples (js)](structures_controle_js/nombres_triples/js/nombres_triples.js)
- [Test du projet nombres triples](structures_controle_js/nombres_triples/readme.md)
- [arborescence](structures_controle_js/nombres_triples/images/arborescence_nbr_triple.png)

**_-Exo Fibonacci :_**
- [Code suite de fibonacci (html)](structures_controle_js/suite_fibonacci/index.html)
- [Code suite de fibonacci (js)](structures_controle_js/suite_fibonacci/js/fibonacci.js)
- [Test du projet suite de fibonacci](structures_controle_js/suite_fibonacci/readme.md)
- [arborescence](structures_controle_js/suite_fibonacci/images/arborescence_fibonacci.png)

**_-Exo fonction IMC :_** 
- [Code fonction IMC  (html)](structures_controle_js/table_multiplication/index.html)
- [Code fonction IMC  (js)](structures_controle_js/table_multiplication/js/multiplication.js)
- [Test du projet fonction IMC ](structures_controle_js/table_multiplication/readme.md)
- [arborescence](structures_controle_js/table_multiplication/images/arborescence_multi.png)

## 4. Les fonctions en JavaScript :

**_-Exo fonction IMC :_** 
- [Code fonction IMC (html)](4.%20fonctions/calcul_IMC/index.html)
- [Code fonction IMC (js)](4.%20fonctions/calcul_IMC/js/calcul_IMC.js)
- [Test du projet fonction IMC ](4.%20fonctions/calcul_IMC/readme.md)
- [arborescence](4.%20fonctions/calcul_IMC/images/arborescence.png)

**_-Exo fonction calcul du temps de parcours d'un trajet :_**
- [Code fonction calcul du tempsde trajet (html)](4.%20fonctions/calcul_temps_trajet/index.html)
- [Code fonction calcul du tempsde trajet (js)](4.%20fonctions/calcul_temps_trajet/js/calcul_temps_trajet.js)
- [Test du projet fonction calcul du tempsde trajet ](4.%20fonctions/calcul_temps_trajet/readme.md)
- [arborescence](4.%20fonctions/calcul_temps_trajet/images/arborescence.png)

**_-Exo fonction multiple de 3 :_**
- [Code fonction multiple de 3 (html)](4.%20fonctions/nombre_multiple_3/index.html)
- [Code fonction multiple de 3 (js)](4.%20fonctions/nombre_multiple_3/js/multiple_3.js)
- [Test du projet fonction multiple de 3](4.%20fonctions/nombre_multiple_3/readme.md)
- [arborescence](4.%20fonctions/nombre_multiple_3/images/Arborescence.png)

## 5. Les objets en JavaScript : 

**_-Exo objet IMC V1 :_**
- [Code Objet IMC V1 (html)](5.%20Objet/calcul_imv_v1/index.html)
- [Code Objet IMC V1 (js)](5.%20Objet/calcul_imv_v1/js/calcul_imc_v1.js)
- [Test du projet Objet IMC V1](5.%20Objet/calcul_imv_v1/readme.md)
- [arborescence](5.%20Objet/calcul_imv_v1/images/arborescence.png)

**_-Exo objet IMC V2 :_**
* [Code Objet IMC V2 (html)](5.%20Objet/calcul_imc_v2/index.html)
* [Code Objet IMC V2 (js)](5.%20Objet/calcul_imc_v2/js/calcul_imc_v2.js)
* [Test du projet Objet IMC V2](5.%20Objet/calcul_imc_v2/readme.md)
* [arborescence](5.%20Objet/calcul_imc_v2/images/arborescence.png)

**_-Exo objet IMC V3 :_**
* [Code Objet IMC V3 (html)](5.%20Objet/calcul_imc_v3/index.html)
* [Code Objet IMC V3 (js)](5.%20Objet/calcul_imc_v3/js/calcul_imc_v3.js)
* [Test du projet Objet IMC V3](5.%20Objet/calcul_imc_v3/readme.md)
* [arborescence](5.%20Objet/calcul_imc_v3/images/test.png)

## 5. Les tableaux en JavaScript : 

**_-Exo tableaux calcul moyenne :_**
* [Code tableaux moyenne (html)](6.%20Tableau/calcul_moyenne/index.html)
* [Code tableaux (js)](6.%20Tableau/calcul_moyenne/js/moyenne.js)
* [Test du projet tableaux calcul de moyenne](6.%20Tableau/calcul_moyenne/readme.md)
* [arborescence](6.%20Tableau/calcul_moyenne/images/arborescence.png)

**_-Exo tableaux liste d'article :_**
* [Code tableaux liste d'article (html)](6.%20Tableau/Liste_Articles/index.html)
* [Code tableaux liste d'article (js)](6.%20Tableau/Liste_Articles/js/liste_articles.js)
* [Test du projet tableaux liste d'article](6.%20Tableau/Liste_Articles/readme.md)
* [arborescence](6.%20Tableau/Liste_Articles/images/arborescence.png)