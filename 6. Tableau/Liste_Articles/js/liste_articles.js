let listeArticles = [
    ["Jus d'orange 1L", 2, 1.35],
    ['Yaourt nature 4X', 1, 1.6],
    ['Pain de mie 500g', 1, 0.9],
    ['Barquette jambon blanc 4xT', 1, 2.75],
    ['Salade Laitue', 1, 0.8],
    ['Spaghettis 500g', 2, 0.95]
];

let achat = 0;
let achatTotal = 0;
let nbr = 0;


console.log("Voici la liste d'articles : ");

for (let articles of listeArticles) {
    console.log(articles[0]);
    achat = articles[1] * articles[2];
    console.log(articles[1] + " x " + articles[2] + " EUR       " + achat.toFixed(1));

    nbr = nbr + articles[1];
    achatTotal = achatTotal + achat;
}

console.log("\nNombre d'articles achetés: " + nbr);
console.log("\nMONTANT: " + achatTotal.toFixed(2) + "EUR");