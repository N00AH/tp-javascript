let listeValeurs = [10, 25, 41, 5, 9, 11];
let somme = 0;

for (let nombre of listeValeurs) {
    console.log(nombre);
}

for (let i = 0; i < listeValeurs.length; i++) {
    somme = somme + listeValeurs[i];
}
console.log("Somme des valeurs : " + somme);
console.log("Moyenne des valeurs : " + (somme / listeValeurs.length).toFixed(2));