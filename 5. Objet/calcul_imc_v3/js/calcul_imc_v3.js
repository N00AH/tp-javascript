function patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {

    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
    this.taille = prmTaille; //cm
    this.poids = prmPoids; //kg

}

patient.prototype.decrire = function() {
    let genre = this.sexe;
    let description = "";
    if (genre == 'masculin') {
        description = "le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. il mesure " + this.taille + " cm et pèse " + this.poids + " kg";

    } else if (genre == 'feminin') {
        description = "la patiente " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agée de " + this.age + " ans. elle mesure " + this.taille + " cm et pèse " + this.poids + " kg";
    }

    return description;
}

patient.prototype.definir_corpulence = function(prmObj) {


    function CalculerImc() {
        let valIMC = 0;
        valIMC = prmObj.poids / ((prmObj.taille / 100) * (prmObj.taille / 100));
        return valIMC;
    }
    let imc = CalculerImc().toFixed(2);
    let genre = prmObj.sexe;

    function interpreter_IMC(prmIMC) {
        let interpretation = "";
        if (genre == 'masculin') {
            if (prmIMC < 16.5) {

                interpretation = "Dénutrition";
            } else if (prmIMC < 18.5) {

                interpretation = "Maigreur";
            } else if (prmIMC < 25) {

                interpretation = "Corpulence normale";
            } else if (prmIMC < 30) {

                interpretation = "Surpoids";
            } else if (prmIMC < 35) {

                interpretation = "Obésité modérée";
            } else if (prmIMC < 40) {

                interpretation = "Obésité sévère";
            } else {

                interpretation = " Obésité morbide";
            }
        } else if (genre == 'feminin') {
            if (prmIMC < 14.5) {

                interpretation = "Dénutrition";
            } else if (prmIMC < 16.5) {

                interpretation = "Maigreur";
            } else if (prmIMC < 23) {

                interpretation = "Corpulence normale";
            } else if (prmIMC < 28) {

                interpretation = "Surpoids";
            } else if (prmIMC < 33) {

                interpretation = "Obésité modérée";
            } else if (prmIMC < 38) {

                interpretation = "Obésité sévère";
            } else {

                interpretation = " Obésité morbide";
            }
        }
        return interpretation;

    }
    let interpret = interpreter_IMC(imc);
    let affichage = "Son IMC est de: " + imc + " \nle patient et donc en situation de " + interpret;
    return affichage;
}

let objPatient = new patient('Dupond', 'Jean', 30, 'masculin', 180, 85);
let objPatient2 = new patient('Moulin', 'Isabelle', 46, 'feminin', 158, 74);
let objPatient3 = new patient('Martin', 'Eric', 42, 'masculin', 165, 90);

console.log(objPatient.decrire());
console.log(objPatient.definir_corpulence(objPatient));

console.log(objPatient2.decrire());
console.log(objPatient2.definir_corpulence(objPatient2));

console.log(objPatient3.decrire());
console.log(objPatient3.definir_corpulence(objPatient3));