let objPatient = { //création de l'objet et de ses propriétes 
    nom: 'Dupond',
    prenom: 'Jean',
    age: 30,
    sexe: 'masculin',
    taille: 180,
    poids: 85,

    decrire: function() { //Fonction pour décrire le patient
        let description;
        description = "le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + this.taille + " cm et pèse " + this.poids + " Kg";
        return description;
    },

    decrireIMC: function() { //Fonction pour décrire et interpreter l'imc

        function calculer_IMC() {
            let valIMC = 0;
            valIMC = objPatient.poids / ((objPatient.taille / 100) * (objPatient.taille / 100));
            return valIMC;
        }
        let imc = calculer_IMC().toFixed(1);

        function interpreter_IMC(prmIMC) {
            let interpretation = "";
            if (prmIMC < 16.5) {

                interpretation = "Dénutrition";
            } else if (prmIMC < 18.5) {

                interpretation = "Maigreur";
            } else if (prmIMC < 25) {

                interpretation = "Corpulence normale";
            } else if (prmIMC < 30) {

                interpretation = "Surpoids";
            } else if (prmIMC < 35) {

                interpretation = "Obésité modérée";
            } else if (prmIMC < 40) {

                interpretation = "Obésité sévère";
            } else {

                interpretation = " Obésité morbide";
            }
            return interpretation;
        }
        let interpret = interpreter_IMC(imc);
        let affichage = "Son IMC est de: " + imc + " \nIl est en situation de " + interpret;
        return affichage;
    },

};

console.log(objPatient.decrire()); //Affichage de la description du patient 
console.log(objPatient.decrireIMC()); //Affichage de l'imc et sont inerpretation