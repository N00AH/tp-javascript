let n = 17; //Variable n avec n unnombre choisi
let cpt = 0; //compteur
let valeurs = "" + n; //Variable valeurs
let valmax = n; //Variable valmax 

console.log("Conjecture de Syracuse pour " + n + ":"); //Affichage de n

if (n < 1) { //Condition si 
    console.log("Erreur n est inférieur à 0");

} else {
    while (n > 1) {
        if (n > valmax) {
            valmax = n;
        }
        if (n % 2 == 0) {
            n = n / 2;
        } else {
            n = (n * 3) + 1;
        }
        valeurs = valeurs + "-" + n;
        cpt++;
    }
}
console.log(valeurs); //Affichage de valeurs
console.log("Temps de vol = " + cpt); //Affichage du compteur
console.log("Altitude max = " + valmax); //Affichage de la valeur max