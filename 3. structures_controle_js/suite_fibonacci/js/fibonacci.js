let n1 = 0;
let n2 = 1;
let somme = 0;
let display = "" + n2;

for (let i = 2; i < 17; i++) {

    somme = n1 + n2; //somme des deux derniers nombres
    n1 = n2; //assigner la dernière valeur à la première
    n2 = somme; //attribue la somme au dernier nombre
    display = display + " " + n2;
}
console.log(display);