console.log("1 euro(s) = " + "1.65 dollar(s)");
for (let i = 2; i <= 16384; i = i * 2) {
    console.log(i + " euro(s) = " + (i * 1.65).toFixed(2) + " dollar(s)"); //Affiche le résultat et converti les euros en dollars avec *1.65 et limite 2 chiffres après la virgule
}