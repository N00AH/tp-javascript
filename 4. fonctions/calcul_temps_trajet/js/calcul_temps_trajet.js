function calculerTempsParcoursSec(prmVitesse, prmDistance) { //Fonction pour calculer le temps de parcours en seconde
    let
        returnVal = prmDistance / prmVitesse * 3600; //Calcul du temps de parcours en seconde
    return returnVal;
}

function convertir_h_min_sec(prmTempenSec) { //Fonction pour convertir les secondes en heures minutes secondes 
    let heures;
    let minutes;
    let secondes;
    let returnVal;
    heures = Math.floor(prmTempenSec / 3600);
    minutes = Math.floor((prmTempenSec / 60) - (heures * 60));
    secondes = Math.floor(prmTempenSec - (minutes * 60) - (heures * 3600))
    returnVal = heures + "h " + minutes + "min " + secondes + "s";
    return returnVal;
}
let vitesse = 90;
let distance = 500;
console.log("Calcul du temps de parcours d'un trajet"); //Affichage du résultat
console.log("Vitesse moyenne (en km/h): " + vitesse);
console.log("Distance (en km): " + distance);
console.log("A " + vitesse + " km/h, une distance de " + distance + " km est parcourue en " + calculerTempsParcoursSec(vitesse, distance) + " s");
tempensec = calculerTempsParcoursSec(vitesse, distance);
console.log("A " + vitesse + " km/h, une distance de " + distance + " km est parcourue en " + convertir_h_min_sec(tempensec));