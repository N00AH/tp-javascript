function rechercher_Mult3(prmMax) { //Fonction recherche multiple de 3
    let returnvalue = "0";
    for (let index = 1; index < prmMax; index++) {
        if (index % 3 == 0) {
            returnvalue = returnvalue + "-" + index;
        }
    }
    return returnvalue;
}
console.log("Recherches des multiples de 3 :");
let valeurlimites = 20;
console.log("Valeur limite de la recherhce: " + valeurlimites);
console.log("Multiples de 3 : " + rechercher_Mult3(valeurlimites));